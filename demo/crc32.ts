/* CRC32
 * Leonardone @ NEETSDKASU
 * MIT License
 */

class CRC32 {
	private static readonly table: Uint32Array = (() => {
		const t = new Uint32Array(256);
		for (let n = 0; n < 256; n++) {
			let c = n;
			for (let k = 0; k < 8; k++) {
				if ((c & 0x1) !== 0) {
					c = 0xedb88320 ^ (c >>> 1);
				} else {
					c >>>= 1;
				}
			}
			t[n] = c;
		}
		return t;
	})();

	public static update_crc(crc: number, buf: number | ArrayBufferLike | ArrayBufferView): number {
		crc ^= 0xffffffff;
		if (typeof buf === 'number') {
			crc = CRC32.table[(crc ^ buf) & 0xff] ^ (crc >>> 8);
		} else {
			const arr = ArrayBuffer.isView(buf)
					  ? new Uint8Array(buf.buffer, buf.byteOffset, buf.byteLength)
					  : new Uint8Array(buf);
			for (let i = 0; i < arr.length; i++) {
				crc = CRC32.table[(crc ^ arr[i]) & 0xff] ^ (crc >>> 8);
			}
		}
		return crc ^ 0xffffffff;
	}

	public static crc(buf: ArrayBufferLike | ArrayBufferView): number {
		return CRC32.update_crc(0, buf);
	}

	private readonly value: Uint32Array;

	public constructor() {
		this.value = new Uint32Array(1);
	}

	public reset(): void {
		this.value[0] = 0;
	}

	public update(buf: number | ArrayBufferLike | ArrayBufferView): void {
		this.value[0] = CRC32.update_crc(this.value[0], buf);
	}

	public getValue32(): number {
		return this.value[0];
	}

	public getValue(): bigint {
		return BigInt(this.value[0]);
	}
}